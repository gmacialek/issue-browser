import { browser, by, element } from 'protractor';
import {WebdriverWebElement} from 'protractor/built/element';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  navigateToUrl(url) {
    return browser.get(browser.baseUrl + url) as Promise<any>;
  }

  getTitleText() {
    return element(by.css('body > app-root > main > app-home > p')).getText() as Promise<string>;
  }

  getSearchText() {
    return element(by.css('app-search-results-list > p')).getText() as Promise<string>;
  }

  getSearchInput() {
    return element(by.css('#mat-input-0')).getWebElement();
  }

  getMatCardTitle() {
    return element(by.css('mat-card:nth-child(1) > mat-card-header > div.mat-card-header-text > mat-card-title > a')).getText() as Promise<string>;
  }
}
