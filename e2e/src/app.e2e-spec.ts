import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Welcome on issue browser, to search for open issues go to Search page.');
  });

  it('should display search message', () => {
    page.navigateToUrl('search');
    expect(page.getSearchText()).toEqual('Your search results will appear here');
  });

  it('should display search box', () => {
    page.navigateToUrl('search');
    page.getSearchInput().sendKeys('test').then(() => {
      browser.driver.sleep(5000).then( () => {
        expect(page.getMatCardTitle().then((text) => text.toLowerCase())).toContain('test');
      });
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
