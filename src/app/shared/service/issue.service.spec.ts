import { TestBed } from '@angular/core/testing';

import { IssueService } from './issue.service';
import {HttpClientModule} from '@angular/common/http';

describe('IssueService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ],
    providers: [IssueService]
  }));

  it('should be created', () => {
    const service: IssueService = TestBed.get(IssueService);
    expect(service).toBeTruthy();
  });

  it('should edit search query', () => {
    const service: IssueService = TestBed.get(IssueService);
    service.editSearchQuery('test');
    expect(service.searchQuery.getValue()).toEqual('test');
  });

  it('should not edit search query', () => {
    const service: IssueService = TestBed.get(IssueService);
    service.editSearchQuery('');
    expect(service.searchQuery.getValue()).toEqual('');
  });

  it('should call httpClient get', () => {
    const service: IssueService = TestBed.get(IssueService);
    const obj = service.getIssues('test');
    expect(obj).toBeTruthy();
  });


});
