import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchBoxComponent } from './search-box.component';
import {Component} from '@angular/core';
import {IssueService} from '../../shared/service/issue.service';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from '../../app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {MatCardModule, MatChipsModule, MatIconModule, MatInputModule, MatProgressSpinnerModule} from '@angular/material';

describe('SearchBoxComponent', () => {
  let component: SearchBoxComponent;
  let fixture: ComponentFixture<SearchBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      declarations: [
        SearchBoxComponent,
        MockMatFormFieldComponent
      ],
      providers: [IssueService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call', () => {
    const service: IssueService = TestBed.get(IssueService);
    spyOn(service, 'editSearchQuery');
    fixture = TestBed.createComponent(SearchBoxComponent);
    component = fixture.componentInstance;
    component.applyFilter({target: {value: 'Test'}});
  });
});

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mat-form-field',
  template: ''
})
class MockMatFormFieldComponent {
}

