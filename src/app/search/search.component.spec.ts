import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import {Component} from '@angular/core';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SearchComponent,
        MockSearchBoxComponent,
        MockSearchResultsListComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call constructor', () => {
    component = new SearchComponent();
    expect(component).toBeTruthy();
  });

  it('should call ngOnInit', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });
});

@Component({
  selector: 'app-search-box',
  template: ''
})
class MockSearchBoxComponent {
}

@Component({
  selector: 'app-search-results-list',
  template: ''
})
class MockSearchResultsListComponent {
}

