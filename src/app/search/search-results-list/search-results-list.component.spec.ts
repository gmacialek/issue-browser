import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchResultsListComponent } from './search-results-list.component';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from '../../app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {MatCardModule, MatChipsModule, MatIconModule, MatInputModule, MatProgressSpinnerModule} from '@angular/material';
import {IssueService} from '../../shared/service/issue.service';
import {HomeComponent} from '../../home/home.component';
import {SearchComponent} from '../search.component';
import {Component} from '@angular/core';

describe('SearchResultsListComponent', () => {
  let component: SearchResultsListComponent;
  let fixture: ComponentFixture<SearchResultsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchResultsListComponent, HomeComponent, SearchComponent, MockSearchBoxComponent],
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        MatInputModule,
        MatCardModule,
        MatChipsModule,
        MatIconModule,
        MatProgressSpinnerModule
      ],
      providers: [IssueService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should save in local storage', () => {
    component.saveInLocalStorage({id: 1, title: 'Test'});
    expect(component).toBeTruthy();
    expect(localStorage.getItem('1')).toBeTruthy();
  });

  it('should exist in local storage', () => {
    component.saveInLocalStorage({id: 1, title: 'Test'});
    const test = component.existInLocalStorage({id: 1, title: 'Test'});
    expect(test).toEqual('Test');
  });

  it('should remove from local storage', () => {
    component.saveInLocalStorage({id: 1, title: 'Test'});
    component.removeFromLocalStorage({id: 1, title: 'Test'});
    expect(localStorage.getItem('1')).toBeFalsy();
  });
});

@Component({
  selector: 'app-search-box',
  template: ''
})
class MockSearchBoxComponent {
}
